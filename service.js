const fetch = require("node-fetch");
const request = require("./request");
const util = require("util");
const mysql = require("mysql");

module.exports = service = {
  fetch100: async () => {
    return await fetch("http://127.0.0.1:8000/data/")
      .then(response => response.json())
      .then(
        response => {
          if ("error" in response) throw new Error(response.error.message);
          return response;
        },
        error => console.error("Network error", error)
      );
  },
  getLastPrice: async () => {
    const result = await request("GET", "instrument", {
      symbol: "XBTUSD",
      count: 1
    });
    console.log(`get last price: ${result[0]["lastPrice"]}`);
    console.log(
      `get last price result: ${JSON.stringify(
        result[0]["lastPrice"],
        null,
        2
      )}`
    );
    return result[0]["lastPrice"];
  },
  getWallet: async () => {
    const result = await request("GET", "user/margin");
    return {
      walletBalance: result["walletBalance"],
      unrealisedPnl: result["unrealisedPnl"],
      availableMargin: result["availableMargin"],
      xRatelimitRemaining: result["x-ratelimit-remaining"],
      xRatelimitReset: result["x-ratelimit-reset"]
    };
  },
  order: async (side, price, orderQty) => {
    console.log(`order: ${side}, ${price}, ${orderQty}`);
    try {
      const data = await request("POST", "order", {
        ordType: "Limit",
        orderQty: Number(orderQty),
        price: Number(price),
        side: side,
        symbol: "XBTUSD"
      });
      if (data["orderID"]) {
        console.log(`submit order done`);
        const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
        await wait(10000);
        request("DELETE", "order", {
          orderID: data["orderID"]
        });
      }
    } catch (error) {
      console.error(error);
    }
  },
  orderMarket: async (side, orderQty) => {
    return await request("POST", "order", {
      ordType: "Market",
      orderQty: orderQty,
      side: side,
      symbol: "XBTUSD"
    });
  },
  cancelOrder: async timeout => {
    return await request("POST", "order/cancelAllAfter", {
      timeout: timeout
    });
  },
  //   cancelOrder: async orderID => {
  //     return await request("DELETE", "order", {
  //       orderID
  //     });
  //   },
  getPositionEntryPrice: async () => {
    const position = await request("GET", "position");
    return position[0]["avgEntryPrice"];
  },
  closePositionLimit: async price => {
    return await request("POST", "order", {
      execInst: "Close",
      ordType: "Limit",
      price: price,
      symbol: "XBTUSD"
    });
  },
  closePositionMarket: async () => {
    console.log(`closePositionMarket`);
    return await request("POST", "order/closePosition/", {
      // execInst: "Close",
      // ordType: "Market",
      symbol: "XBTUSD"
    });
  },
  setLeverage: async leverage => {
    return await request("POST", "leverage", {
      leverage: leverage
    });
  },
  getSignals: async () => {
    const connection = mysql.createConnection({
      host: "localhost",
      user: "root",
      password: "2PV8dgZUWZ",
      database: "bitmex"
    });
    // const connection = mysql.createConnection({
    //   host: "localhost",
    //   user: "root",
    //   password: "gun1553960402",
    //   database: "test"
    // });
    const query = util.promisify(connection.query).bind(connection);
    try {
      return await query(
        "select `signal` from `1hour` order by `timestamp` desc limit 1"
      );
    } catch (error) {
      throw error;
    } finally {
      connection.end();
    }
  }
};
