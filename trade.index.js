(async function trade() {
  const moment = require("moment");
  const service = require("./service");

  const signals = await service.getSignals();
  const signal = signals[0]["signal"];
  const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

  tradeIn15m(signal, service);
  await wait(15 * 60 * 1000);
  await service.closePositionMarket();
  return;
})();
function tradeIn15m(signal, service) {
  const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
  const id = setInterval(tradeEvery10s, 10000, signal);
  const cancelTrade10s = () => clearInterval(id);
  setTimeout(cancelTrade10s, 14 * 60 * 1000);
  async function tradeEvery10s(signal) {
    const wallet = await service.getWallet();
    const price = await service.getLastPrice();
    const { unrealisedPnl, walletBalance, availableMargin } = wallet;
    const qty = Math.floor(
      (Number(availableMargin) / 100000000) * Number(price)
    );
    const lost = unrealisedPnl / walletBalance;
    if (lost <= -0.1) {
      console.log("losting");
      await service.closePositionMarket();
      tradeEvery10s(signal);
      return;
    }
    switch (signal) {
      case "buy":
        console.log(`buy: price: ${price}, qty: ${qty}`);
        if (
          (availableMargin === 0 &&
            walletBalance > 0 &&
            walletBalance / 100000000 < 1 / price) ||
          (availableMargin > 0 && availableMargin / 100000000 < 1 / price)
        ) {
          console.log("balance limited");
          await wait(2000);
          tradeEvery10s(signal);
          return;
        }
        // await Promise.all([
        //   service.order("Buy", price - 0.5, Math.round(qty * 0.3)),
        //   service.order("Buy", price - 1, Math.round(qty * 0.7))
        // ]);
        await service.order("Buy", price - 0.5, Math.round(qty * 0.3));
        await service.order("Buy", price - 1, Math.round(qty * 0.7));
        tradeEvery10s(signal);
        break;
      case "sell":
        console.log(`sell: price: ${price}, qty: ${qty}`);
        if (
          (availableMargin === 0 &&
            walletBalance > 0 &&
            walletBalance / 100000000 < 1 / price) ||
          (availableMargin > 0 && availableMargin / 100000000 < 1 / price)
        ) {
          console.log("balance limited");
          await wait(2000);
          tradeEvery10s(signal);
          return;
        }
        // await Promise.all([
        //   service.order("Sell", price + 0.5, Math.round(qty * 0.3)),
        //   service.order("Sell", price + 1, Math.round(qty * 0.7))
        // ]);
        await service.order("Sell", price + 0.5, Math.round(qty * 0.3));
        await service.order("Sell", price + 1, Math.round(qty * 0.7));
        tradeEvery10s(signal);
        break;
      case "市价平仓":
        console.log("市价平仓");
        await service.closePositionMarket();
        tradeEvery10s(signal);
        break;
      case "不操作":
        console.log("不操作");
      default:
        console.log("default: ", signal);
        break;
    }
  }
}
