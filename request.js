const fetch = require("node-fetch");
const crypto = require("crypto");
const qs = require("qs");

module.exports = function request(verb, endpoint, data = {}) {
  const apiKey = "A3Fj9mERr3OuQNNxo5y_vv9j";
  const apiSecret = "e4WF3azv91NaqOokVE7fYk3s7Z7K6CFnWsMXvQ6hf08Qm-kq";

  const apiRoot = "/api/v1/";

  const expires = new Date().getTime() + 60 * 1000; // 1 min in the future

  let query = "",
    postBody = "";
  if (verb === "GET") query = "?" + qs.stringify(data);
  // Pre-compute the reqBody so we can be sure that we're using *exactly* the same body in the request
  // and in the signature. If you don't do this, you might get differently-sorted keys and blow the signature.
  else postBody = JSON.stringify(data);

  const signature = crypto
    .createHmac("sha256", apiSecret)
    .update(verb + apiRoot + endpoint + query + expires + postBody)
    .digest("hex");

  const headers = {
    "content-type": "application/json",
    accept: "application/json",
    // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
    // https://www.bitmex.com/app/apiKeysUsage for more details.
    "api-expires": expires,
    "api-key": apiKey,
    "api-signature": signature
  };

  const requestOptions = {
    method: verb,
    headers
  };
  if (verb !== "GET") requestOptions.body = postBody; // GET/HEAD requests can't have body

  const url = "https://testnet.bitmex.com" + apiRoot + endpoint + query;
  return fetch(url, requestOptions)
    .then(response => response.json())
    .then(
      async response => {
        if ("error" in response) {
          throw new Error(response.error.message);
        }
        if (
          typeof response.error !== "undefined" &&
          typeof response.error.name !== "undefined" &&
          response.error.name === "RateLimitError"
        ) {
          console.log(`RateLimitError response: `, response);
          const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
          await wait(2000);
          // if (
          //   typeof response["x-ratelimit-reset"] !== "undefined" &&
          //   typeof response["x-ratelimit-remaining"] !== "undefined"
          // ) {
          //   console.log(
          //     `response["x-ratelimit-reset"]: `,
          //     response["x-ratelimit-reset"]
          //   );
          //   console.log(
          //     `response["x-ratelimit-remaining"]: `,
          //     response["x-ratelimit-remaining"]
          //   );
          //   await wait(
          //     (response["x-ratelimit-reset"] - moment().unix()) * 1000
          //   );
          // }
        }

        return response;
      },
      error => console.error("Network error", error)
    )
    .catch(e => {
      console.error(`
      ${e}
      ${url}
      ${JSON.stringify(requestOptions, null, 2)}
      `);
    });
};
