// buy
// sell
// 市价平仓
// 不操作

const service = require("./service");
const moment = require("moment");

module.exports = async function trade(action) {
  console.log("start trading");
  console.log(`action: ${action}`);
  const wallet = await service.getWallet();
  const price = await service.getLastPrice();
  const { unrealisedPnl, walletBalance, availableMargin } = wallet;
  const lost = unrealisedPnl / walletBalance;

  const qty = Math.floor((Number(availableMargin) / 100000000) * Number(price));
  //   if (moment().minutes() >= 15) {
  //     if (moment().minutes() === 59 && moment().seconds() === 49) {
  //       await service.cancelOrder(10000);
  //     }
  //     await service.orderMarket(`${action[0].toUpperCase()}${action.slice(1)}`, qty);
  //   }

  const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

  if (lost < -0.1) {
    console.log("losting");
    // const entryPrice = await service.getPositionEntryPrice();
    // if (Number(entryPrice) > 0) {
    //   await service.closePositionMarket();
    // }
    await service.closePositionMarket();
    trade(action);
  } else if (
    (availableMargin === 0 && walletBalance > 0 && walletBalance / 100000000 < 1 / price) ||
    (availableMargin > 0 && availableMargin / 100000000 < 1 / price)
  ) {
    const entryPrice = await service.getPositionEntryPrice();
    const wallet = await service.getWallet();
    const price = await service.getLastPrice();
    const { unrealisedPnl, walletBalance, availableMargin } = wallet;
    if (
      walletBalance <= 0 ||
      walletBalance / 100000000 < 1 / price ||
      (availableMargin > 0 && availableMargin / 100000000 < 1 / price)
    ) {
      console.log("balance limited");
      trade(action);
    } else if (Number(entryPrice) > 0) {
      trade(action);
    } else {
      console.log("???");
      trade(action);
    }
  } else {
    console.log(`not losting: ${lost}`);
    switch (action) {
      case "buy":
        console.log(`buy: price: ${price}, qty: ${qty}`);
        await Promise.all([
          service.order("Buy", price - 0.5, Math.round(qty * 0.3)),
          service.order("Buy", price - 1, Math.round(qty * 0.7))
        ]);
        // await cancel(trade, action);
        // await wait(10000);
        trade(action);
        break;
      case "sell":
        await Promise.all([
          service.order("Sell", price + 0.5, Math.round(qty * 0.3)),
          service.order("Sell", price + 1, Math.round(qty * 0.7))
        ]);
        // await cancel(trade, action);
        // await wait(10000);
        trade(action);
        break;
      case "市价平仓":
        await service.closePositionMarket();
        //   trade(action);
        break;
      case "不操作":
      default:
        break;
    }
  }

  async function cancel(trade, action) {
    try {
      const cancel = await service.cancelOrder(10000);
      console.log(`cancel["cancelTime"]: ${cancel["cancelTime"]}`);
      return cancel["cancelTime"];
    } catch (error) {
      console.error(error);
    }
  }
};
