const calculate = require("./calculate");

module.exports = function calculateMore(data) {
  const toReplace = data.findIndex(
    x => x["rsv"] === "" || x["rsv"] === null || typeof x["rsv"] === "undefined"
  );
  if (toReplace === -1) {
    // console.log(`final: ${JSON.stringify(data[29])}`);
    console.log(`nothing to replace`);
    return;
  } else {
    console.log(`toReplace: ${toReplace}`);
    // console.log(`${JSON.stringify(data[toReplace])}`);
    const result = calculate(data.slice(0, toReplace + 1));
    // console.log(`result 1: ${JSON.stringify(result)}`);
    data[toReplace] = result;
    calculateMore(data);
  }
  return data;
};
