module.exports = function calculate(data) {
  console.log(`start calculating`);
  var current = data[data.length - 1];
  var previous = data[data.length - 2];

  // console.log(`previous: ${JSON.stringify(previous)}`);
  // console.log(`data: ${JSON.stringify(data)}`);
  // console.log(`data length : ${data.length}`);

  current["MaxIn13"] =
    previous["MaxIn13"] > current["high"]
      ? previous["MaxIn13"]
      : current["high"];
  current["MinIn13"] =
    previous["MinIn13"] < current["low"] ? previous["MinIn13"] : current["low"];
  current["rsv"] =
    (-(current["MaxIn13"] - current["close"]) /
      (current["MaxIn13"] - current["MinIn13"])) *
    100;
  current["lwr1"] = (current["rsv"] + 2 * previous["lwr1"]) / 3;
  current["lwr2"] = (current["lwr1"] + 2 * previous["lwr2"]) / 3;
  current["action1-1"] = current["lwr1"] >= current["lwr2"] ? "buy" : "sell";
  current["MaxIn8"] =
    previous["MaxIn8"] > current["high"] ? previous["MaxIn8"] : current["high"];
  current["MinIn8"] =
    previous["MinIn8"] < current["low"] ? previous["MinIn8"] : current["low"];
  current["rsv1"] =
    ((current["close"] - current["MinIn8"]) /
      (current["MaxIn8"] - current["MinIn8"])) *
    100;
  current["k"] = (current["rsv1"] + 2 * previous["k"]) / 3;
  current["d"] = (current["k"] + 2 * previous["d"]) / 3;
  current["action1-2"] = current["k"] >= current["d"] ? "buy" : "sell";
  current["lc"] = previous["close"];
  current["rsi1_1"] =
    (Math.max(current["close"] - current["lc"], 0) + 4 * previous["rsi1_1"]) /
    5;
  current["rsi1_2"] =
    (Math.abs(current["close"] - current["lc"]) + 4 * previous["rsi1_2"]) / 5;
  current["rsi1"] = (current["rsi1_1"] / current["rsi1_2"]) * 100;
  current["rsi2_1"] =
    (Math.max(current["close"] - current["lc"], 0) + 12 * previous["rsi2_1"]) /
    13;
  current["rsi2_2"] =
    (Math.abs(current["close"] - current["lc"]) + 12 * previous["rsi2_2"]) / 13;
  current["rsi2"] = (current["rsi2_1"] / current["rsi2_2"]) * 100;
  current["action1-3"] = current["rsi1"] >= current["rsi2"] ? "buy" : "sell";
  current["diff_1"] = (2 * current["close"] + 7 * previous["diff_1"]) / 9;
  current["diff_2"] = (2 * current["close"] + 12 * previous["diff_2"]) / 14;
  current["diff"] = current["diff_1"] - current["diff_2"];
  current["dea"] = (2 * current["diff"] + 4 * previous["dea"]) / 6;
  current["action1-4"] = current["diff"] >= current["rsa"] ? "buy" : "sell";
  data.map(function(x, i, array) {
    if (i > 1 && i === array.length - 1) {
      let sum = 0;
      for (let n = i; n > i - 3; n--) {
        sum += Number(array[n]["close"]);
      }
      x["bbi_1"] = sum / 3;
    }
  });
  data.map(function(x, i, array) {
    if (i > 3 && i === array.length - 1) {
      let sum = 0;
      for (let n = i; n > i - 5; n--) {
        sum += Number(array[n]["close"]);
      }
      x["bbi_2"] = sum / 5;
    }
  });
  data.map(function(x, i, array) {
    if (i > 6 && i === array.length - 1) {
      let sum = 0;
      for (let n = i; n > i - 8; n--) {
        sum += Number(array[n]["close"]);
      }
      x["bbi_3"] = sum / 8;
    }
  });
  data.map(function(x, i, array) {
    if (i > 11 && i === array.length - 1) {
      let sum = 0;
      for (let n = i; n > i - 13; n--) {
        sum += Number(array[n]["close"]);
      }
      x["bbi_4"] = sum / 13;
    }
  });

  current["bbi"] =
    (current["bbi_1"] +
      current["bbi_2"] +
      current["bbi_3"] +
      current["bbi_4"]) /
    4;
  current["action1-5"] = current["close"] >= current["bbi"] ? "buy" : "sell";
  current["mtm"] = current["close"] - current["lc"];
  current["mms_1_1"] = (2 * current["mtm"] + 4 * previous["mms_1_1"]) / 6;
  current["mms_1"] = (2 * current["mms_1_1"] + 2 * previous["mms_1"]) / 4;
  current["mms_2_1"] =
    (2 * Math.abs(current["mtm"]) + 4 * previous["mms_2_1"]) / 6;
  current["mms_2"] = (2 * current["mms_2_1"] + 2 * previous["mms_2"]) / 4;
  current["mms"] = 100 * (current["mms_1"] / current["mms_2"]);
  current["mmm_1_1"] = (2 * current["mtm"] + 12 * previous["mmm_1_1"]) / 14;
  current["mmm_1"] = (2 * current["mmm_1_1"] + 7 * previous["mmm_1"]) / 9;
  current["mmm_2_1"] =
    (2 * Math.abs(current["mtm"]) + 12 * previous["mmm_2_1"]) / 14;
  current["mmm_2"] = (2 * current["mmm_2_1"] + 7 * previous["mmm_2"]) / 9;
  current["mmm"] = 100 * (current["mmm_1"] / current["mmm_2"]);
  current["action1-6"] = current["mms"] >= current["mmm"] ? "buy" : "sell";
  current["wj"] = (current["high"] + current["low"] + current["close"]) / 3;
  current["v1"] =
    current["high"] === current["low"]
      ? 1
      : current["high"] - Math.max(current["open"], current["close"]);
  current["v2"] =
    current["high"] === current["low"]
      ? 1
      : Math.max(current["open"], current["close"]) - current["wj"];
  current["v3"] =
    current["high"] === current["low"]
      ? 1
      : Math.min(current["open"], current["close"]) - current["low"];
  current["v4"] =
    current["high"] === current["low"]
      ? 1
      : current["wj"] - Math.min(current["open"], current["close"]);
  current["v5"] =
    current["volume"] /
    (current["high"] === current["low"] ? 4 : current["high"] - current["low"]);
  current["v6"] = current["v1"] * current["v5"];
  current["v7"] = current["v2"] * current["v5"];
  current["v8"] = current["v3"] * current["v5"];
  current["v9"] = current["v4"] * current["v5"];
  current["ddx"] =
    current["v9"] + current["v8"] - (current["v6"] + current["v7"]);

  data.map(function(x, i, array) {
    if (i >= 4 && i === array.length - 1) {
      let sum = 0;
      for (let n = i; n >= i - 4; n--) {
        sum += Number(array[n]["ddx"]);
      }
      x["dx_1"] = sum / 5;
    }
  });

  current["dx"] = current["dx_1"] * 20;
  current["ddx1"] = (current["dx"] + 2 * previous["ddx1"]) / 3;
  current["ddx2"] = (current["ddx1"] + 2 * previous["ddx2"]) / 3;
  current["action1-7"] = current["ddx1"] >= current["ddx2"] ? "buy" : "sell";
  current["huanshou"] = current["volume"] / (current["close"] * 80000000000);
  current["var1"] =
    current["volume"] === 0
      ? 0
      : current["volume"] /
        ((current["high"] - current["low"]) * 2 -
          Math.abs(current["close"] - current["open"]));

  current["maipanA1_1"] =
    current["close"] > current["open"]
      ? current["var1"] *
        (current["high"] -
          current["open"] +
          (current["close"] - current["low"]))
      : current["volume"] / 2;
  current["maipanA1"] =
    current["close"] > current["open"]
      ? (current["high"] - current["low"]) * current["var1"]
      : current["maipanA1_1"];
  current["maipanB1_1"] =
    current["close"] > current["open"]
      ? 0 - current["var1"] * (current["high"] - current["low"])
      : 0 - current["volume"] / 2;
  current["maipanB1"] =
    current["close"] > current["open"]
      ? 0 -
        current["var1"] *
          (current["high"] -
            current["close"] +
            (current["open"] - current["low"]))
      : current["maipanB1_1"];
  current["dlx"] =
    current["volume"] === 0
      ? 0
      : ((current["maipanA1"] - -current["maipanB1"]) / current["volume"]) *
        current["huanshou"];

  data.map(function(x, i, array) {
    if (i > 4 && i === array.length - 1) {
      let sum = 0;
      for (let n = i; n > i - 6; n--) {
        sum += Number(array[n]["dlx"]);
      }
      x["dlx1"] = sum;
    }
  });
  data.map(function(x, i, array) {
    if (i > 8 && i === array.length - 1) {
      let sum = 0;
      for (let n = i; n > i - 5; n--) {
        sum += Number(array[n]["dlx1"]);
      }
      x["dlx2"] = sum / 5;
    }
  });
  current["action1-8"] = current["dlx1"] >= current["dlx2"] ? "buy" : "sell";
  current["mid"] =
    (3 * current["close"] +
      current["low"] +
      current["open"] +
      current["high"]) /
    6;

  data.map(function(x, i, array) {
    if (i > 19 && i === array.length - 1) {
      let sum = 0;
      for (let n = i; n > i - 19; n--) {
        sum += (20 - i + n) * Number(array[n]["mid"]);
      }
      x["zhuli"] = (sum + Number(array[i - 20]["mid"])) / 210;
    }
  });

  current["action2-1"] = current["zhuli"] >= 1 ? "buy" : "sell";

  currentAction1 = [
    current["action1-1"],
    current["action1-2"],
    current["action1-3"],
    current["action1-4"],
    current["action1-5"],
    current["action1-6"],
    current["action1-7"],
    current["action1-8"]
  ];

  const actionA = currentAction1.filter(x => x === "buy").length;
  const actionB = current["action2-1"];

  // console.log(`actionA: ${actionA}`);
  // console.log(`actionB: ${actionB}`);

  current["signal"] =
    actionB === "buy"
      ? setActionBuy(actionA, current)
      : actionB === "sell"
        ? setActionSell(actionA, current)
        : "WTF";
  console.log(`current["action"]: ${current["signal"]}`);
  current["signal"] = "buy";
  console.log(`current["action"]: ${current["signal"]}`);

  return current;
};

function setActionBuy(actionA) {
  if (actionA > 6) {
    return "buy";
  } else if (actionA < 6) {
    return "市价平仓";
  }
  return "不操作";
}

function setActionSell(actionA) {
  if (actionA < 2) {
    return "sell";
  } else if (actionA > 2) {
    return "市价平仓";
  }
  return "不操作";
}
