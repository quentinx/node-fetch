const calculateMore = require("./calculateMore");
const { query } = require("./query");

async function select100() {
  let sql =
    "select * from (select * from `1hour` order by `timestamp` desc limit 100) sub order by `timestamp` asc";
  let dataList = await query(sql);
  return dataList;
}

async function main() {
  try {
    let klines = await select100();
    console.log(`klines length: `, klines.length);
    const result = calculateMore(klines);

    if (result) {
      console.log(`calculate done! result.length: `, result.length);
      console.log(
        `result[result.length - 1]["signal"]: `,
        result[result.length - 1]["signal"]
      );
      result.map(async x => {
        if (x["volume"]) x["volume"] = x["volume"].toString();
        if (x["turnover"]) x["turnover"] = x["turnover"].toString();
        const sql = "INSERT INTO `1hour` (`timestamp`, `open`,`high`,`low`,`close`,`volume`,`turnover`,`MaxIn13`,`MinIn13`,`rsv`,`lwr1`,`lwr2`,`action1-1`,`MaxIn8`,`MinIn8`,`rsv1`,`k`,`d`,`action1-2`,`lc`,`rsi1_1`,`rsi1_2`,`rsi1`,`rsi2_1`,`rsi2_2`,`rsi2`,`action1-3`,`diff_1`,`diff_2`,`diff`,`dea`,`action1-4`,`bbi_1`,`bbi_2`,`bbi_3`,`bbi_4`,`bbi`,`action1-5`,`mtm`,`mms_1_1`,`mms_1`,`mms_2_1`,`mms_2`,`mms`,`mmm_1_1`,`mmm_1`,`mmm_2_1`,`mmm_2`,`mmm`,`action1-6`,`wj`,`v1`,`v2`,`v3`,`v4`,`v5`,`v6`,`v7`,`v8`,`v9`,`ddx`,`dx_1`,`dx`,`ddx1`,`ddx2`,`action1-7`,`huanshou`,`var1`,`maipanA1_1`,`maipanA1`,`maipanB1_1`,`maipanB1`,`dlx`,`dlx1`,`dlx2`,`action1-8`,`mid`,`zhuli`,`action2-1`, `signal`) VALUES('" + x["timestamp"] + "','" + x["open"] + "','" + x["high"] + "','" + x["low"] + "','" + x["close"] + "','" + x["volume"] + "','" + x["turnover"] + "','" + x["MaxIn13"] + "','" + x["MinIn13"] + "','" + x["rsv"] + "','" + x["lwr1"] + "','" + x["lwr2"] + "','" + x["action1-1"] + "','" + x["MaxIn8"] + "','" + x["MinIn8"] + "','" + x["rsv1"] + "','" + x["k"] + "','" + x["d"] + "','" + x["action1-2"] + "','" + x["lc"] + "','" + x["rsi1_1"] + "','" + x["rsi1_2"] + "','" + x["rsi1"] + "','" + x["rsi2_1"] + "','" + x["rsi2_2"] + "','" + x["rsi2"] + "','" + x["action1-3"] + "','" + x["diff_1"] + "','" + x["diff_2"] + "','" + x["diff"] + "','" + x["dea"] + "','" + x["action1-4"] + "','" + x["bbi_1"] + "','" + x["bbi_2"] + "','" + x["bbi_3"] + "','" + x["bbi_4"] + "','" + x["bbi"] + "','" + x["action1-5"] + "','" + x["mtm"] + "','" + x["mms_1_1"] + "','" + x["mms_1"] + "','" + x["mms_2_1"] + "','" + x["mms_2"] + "','" + x["mms"] + "','" + x["mmm_1_1"] + "','" + x["mmm_1"] + "','" + x["mmm_2_1"] + "','" + x["mmm_2"] + "','" + x["mmm"] + "','" + x["action1-6"] + "','" + x["wj"] + "','" + x["v1"] + "','" + x["v2"] + "','" + x["v3"] + "','" + x["v4"] + "','" + x["v5"] + "','" + x["v6"] + "','" + x["v7"] + "','" + x["v8"] + "','" + x["v9"] + "','" + x["ddx"] + "','" + x["dx_1"] + "','" + x["dx"] + "','" + x["ddx1"] + "','" + x["ddx2"] + "','" + x["action1-7"] + "','" + x["huanshou"] + "','" + x["var1"] + "','" + x["maipanA1_1"] + "','" + x["maipanA1"] + "','" + x["maipanB1_1"] + "','" + x["maipanB1"] + "','" + x["dlx"] + "','" + x["dlx1"] + "','" + x["dlx2"] + "','" + x["action1-8"] + "','" + x["mid"] + "','" + x["zhuli"] + "','" + x["action2-1"] + "','" + x["signal"] + "') ON DUPLICATE KEY UPDATE `open`=VALUES(`open`),`high`=VALUES(`high`),`low`=VALUES(`low`),`close`=VALUES(`close`),`volume`=VALUES(`volume`),`turnover`=VALUES(`turnover`),`MaxIn13`=VALUES(`MaxIn13`),`MinIn13`=VALUES(`MinIn13`),`rsv`=VALUES(`rsv`),`lwr1`=VALUES(`lwr1`),`lwr2`=VALUES(`lwr2`),`action1-1`=VALUES(`action1-1`),`MaxIn8`=VALUES(`MaxIn8`),`MinIn8`=VALUES(`MinIn8`),`rsv1`=VALUES(`rsv1`),`k`=VALUES(`k`),`d`=VALUES(`d`),`action1-2`=VALUES(`action1-2`),`lc`=VALUES(`lc`),`rsi1_1`=VALUES(`rsi1_1`),`rsi1_2`=VALUES(`rsi1_2`),`rsi1`=VALUES(`rsi1`),`rsi2_1`=VALUES(`rsi2_1`),`rsi2_2`=VALUES(`rsi2_2`),`rsi2`=VALUES(`rsi2`),`action1-3`=VALUES(`action1-3`),`diff_1`=VALUES(`diff_1`),`diff_2`=VALUES(`diff_2`),`diff`=VALUES(`diff`),`dea`=VALUES(`dea`),`action1-4`=VALUES(`action1-4`),`bbi_1`=VALUES(`bbi_1`),`bbi_2`=VALUES(`bbi_2`),`bbi_3`=VALUES(`bbi_3`),`bbi_4`=VALUES(`bbi_4`),`bbi`=VALUES(`bbi`),`action1-5`=VALUES(`action1-5`),`mtm`=VALUES(`mtm`),`mms_1_1`=VALUES(`mms_1_1`),`mms_1`=VALUES(`mms_1`),`mms_2_1`=VALUES(`mms_2_1`),`mms_2`=VALUES(`mms_2`),`mms`=VALUES(`mms`),`mmm_1_1`=VALUES(`mmm_1_1`),`mmm_1`=VALUES(`mmm_1`),`mmm_2_1`=VALUES(`mmm_2_1`),`mmm_2`=VALUES(`mmm_2`),`mmm`=VALUES(`mmm`),`action1-6`=VALUES(`action1-6`),`wj`=VALUES(`wj`),`v1`=VALUES(`v1`),`v2`=VALUES(`v2`),`v3`=VALUES(`v3`),`v4`=VALUES(`v4`),`v5`=VALUES(`v5`),`v6`=VALUES(`v6`),`v7`=VALUES(`v7`),`v8`=VALUES(`v8`),`v9`=VALUES(`v9`),`ddx`=VALUES(`ddx`),`dx_1`=VALUES(`dx_1`),`dx`=VALUES(`dx`),`ddx1`=VALUES(`ddx1`),`ddx2`=VALUES(`ddx2`),`action1-7`=VALUES(`action1-7`),`huanshou`=VALUES(`huanshou`),`var1`=VALUES(`var1`),`maipanA1_1`=VALUES(`maipanA1_1`),`maipanA1`=VALUES(`maipanA1`),`maipanB1_1`=VALUES(`maipanB1_1`),`maipanB1`=VALUES(`maipanB1`),`dlx`=VALUES(`dlx`),`dlx1`=VALUES(`dlx1`),`dlx2`=VALUES(`dlx2`),`action1-8`=VALUES(`action1-8`),`mid`=VALUES(`mid`),`zhuli`=VALUES(`zhuli`),`action2-1`=VALUES(`action2-1`),`signal`=VALUES(`signal`)";
        let data = await query(sql);
        console.log(`data: `, data);
      });
    }
    return;
  } catch (e) {
    console.error(`something wrong: ${e}`);
  }
}

main();
