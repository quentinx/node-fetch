temp = [
  {
    timestamp: "2015-09-25T13:00:00.000Z",
    open: 239.99,
    high: 239.99,
    low: 237.36,
    close: 237.45,
    volume: 11501,
    turnover: 2730544990
  },
  {
    timestamp: "2015-09-25T14:00:00.000Z",
    open: 237.45,
    high: 237.05,
    low: 236.08,
    close: 236.08,
    volume: 22625,
    turnover: 5344986250
  },
  {
    timestamp: "2015-09-25T15:00:00.000Z",
    open: 236.08,
    high: 236.52,
    low: 236.1,
    close: 236.34,
    volume: 17434,
    turnover: 4119046040
  },
  {
    timestamp: "2015-09-25T16:00:00.000Z",
    open: 236.34,
    high: 236.13,
    low: 235.44,
    close: 235.71,
    volume: 26900,
    turnover: 6341186000
  },
  {
    timestamp: "2015-09-25T17:00:00.000Z",
    open: 235.71,
    high: 236.01,
    low: 235.46,
    close: 235.75,
    volume: 29200,
    turnover: 6885159000
  },
  {
    timestamp: "2015-09-25T18:00:00.000Z",
    open: 235.75,
    high: 236.21,
    low: 235.6,
    close: 235.6,
    volume: 20335,
    turnover: 4797531000
  },
  {
    timestamp: "2015-09-25T19:00:00.000Z",
    open: 235.6,
    high: 236.15,
    low: 235.85,
    close: 236.1,
    volume: 25691,
    turnover: 6064468970
  },
  {
    timestamp: "2015-09-25T20:00:00.000Z",
    open: 236.1,
    high: 236.12,
    low: 235.44,
    close: 235.44,
    volume: 4500,
    turnover: 1061340000
  },
  {
    timestamp: "2015-09-25T21:00:00.000Z",
    open: 235.44,
    high: 235.66,
    low: 235.03,
    close: 235.03,
    volume: 11240,
    turnover: 2644507560
  },
  {
    timestamp: "2015-09-25T22:00:00.000Z",
    open: 235.03,
    high: 235.22,
    low: 234.81,
    close: 234.9,
    volume: 15705,
    turnover: 3689413140
  },
  {
    timestamp: "2015-09-25T23:00:00.000Z",
    open: 234.9,
    high: 235.2,
    low: 234.9,
    close: 235.18,
    volume: 1809,
    turnover: 424985980
  },
  {
    timestamp: "2015-09-26T00:00:00.000Z",
    open: 235.18,
    high: 235.36,
    low: 235.19,
    close: 235.2,
    volume: 3150,
    turnover: 741019740
  },
  {
    timestamp: "2015-09-26T01:00:00.000Z",
    open: 235.2,
    high: 235.29,
    low: 235.29,
    close: 235.29,
    volume: 100,
    turnover: 23529000
  },
  {
    timestamp: "2015-09-26T02:00:00.000Z",
    open: 235.29,
    high: 235.36,
    low: 234.9,
    close: 234.9,
    volume: 2101,
    turnover: 493571310
  },
  {
    timestamp: "2015-09-26T03:00:00.000Z",
    open: 234.9,
    high: 234.9,
    low: 234.9,
    close: 234.9,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-26T04:00:00.000Z",
    open: 234.9,
    high: 234.9,
    low: 234.9,
    close: 234.9,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-26T05:00:00.000Z",
    open: 234.9,
    high: 235.11,
    low: 233.43,
    close: 233.43,
    volume: 7415,
    turnover: 1739140400
  },
  {
    timestamp: "2015-09-26T06:00:00.000Z",
    open: 233.43,
    high: 233.65,
    low: 233.3,
    close: 233.61,
    volume: 9539,
    turnover: 2226613700
  },
  {
    timestamp: "2015-09-26T07:00:00.000Z",
    open: 233.61,
    high: 234.17,
    low: 234.17,
    close: 234.17,
    volume: 70,
    turnover: 16391900
  },
  {
    timestamp: "2015-09-26T08:00:00.000Z",
    open: 234.17,
    high: 234.17,
    low: 234.17,
    close: 234.17,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-26T09:00:00.000Z",
    open: 234.17,
    high: 233.97,
    low: 233.36,
    close: 233.85,
    volume: 13509,
    turnover: 3157068250
  },
  {
    timestamp: "2015-09-26T10:00:00.000Z",
    open: 233.85,
    high: 233.39,
    low: 233.28,
    close: 233.39,
    volume: 4500,
    turnover: 1050090000
  },
  {
    timestamp: "2015-09-26T11:00:00.000Z",
    open: 233.39,
    high: 233.35,
    low: 233.35,
    close: 233.35,
    volume: 1000,
    turnover: 233350000
  },
  {
    timestamp: "2015-09-26T12:00:00.000Z",
    open: 233.35,
    high: 233.85,
    low: 232.91,
    close: 233.85,
    volume: 23572,
    turnover: 5495973000
  },
  {
    timestamp: "2015-09-26T13:00:00.000Z",
    open: 233.85,
    high: 234.87,
    low: 234.1,
    close: 234.68,
    volume: 16625,
    turnover: 3894450750
  },
  {
    timestamp: "2015-09-26T14:00:00.000Z",
    open: 234.68,
    high: 234.35,
    low: 234.34,
    close: 234.34,
    volume: 2050,
    turnover: 480397500
  },
  {
    timestamp: "2015-09-26T15:00:00.000Z",
    open: 234.34,
    high: 235,
    low: 235,
    close: 235,
    volume: 4000,
    turnover: 940000000
  },
  {
    timestamp: "2015-09-26T16:00:00.000Z",
    open: 235,
    high: 235.33,
    low: 234.85,
    close: 234.85,
    volume: 550,
    turnover: 129301000
  },
  {
    timestamp: "2015-09-26T17:00:00.000Z",
    open: 234.85,
    high: 234.47,
    low: 233.5,
    close: 233.74,
    volume: 68830,
    turnover: 16104376500
  },
  {
    timestamp: "2015-09-26T18:00:00.000Z",
    open: 233.74,
    high: 233.8,
    low: 233.68,
    close: 233.79,
    volume: 5000,
    turnover: 1168810000
  },
  {
    timestamp: "2015-09-26T19:00:00.000Z",
    open: 233.79,
    high: 234,
    low: 233.69,
    close: 234,
    volume: 2770,
    turnover: 647918000
  },
  {
    timestamp: "2015-09-26T20:00:00.000Z",
    open: 234,
    high: 234.73,
    low: 234.05,
    close: 234.35,
    volume: 19538,
    turnover: 4581780420
  },
  {
    timestamp: "2015-09-26T21:00:00.000Z",
    open: 234.35,
    high: 234.67,
    low: 234.32,
    close: 234.63,
    volume: 13932,
    turnover: 3266571200
  },
  {
    timestamp: "2015-09-26T22:00:00.000Z",
    open: 234.63,
    high: 234.53,
    low: 234.18,
    close: 234.18,
    volume: 13500,
    turnover: 3164215000
  },
  {
    timestamp: "2015-09-26T23:00:00.000Z",
    open: 234.18,
    high: 234.51,
    low: 234.51,
    close: 234.51,
    volume: 200,
    turnover: 46902000
  },
  {
    timestamp: "2015-09-27T00:00:00.000Z",
    open: 234.51,
    high: 234.51,
    low: 234.51,
    close: 234.51,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-27T01:00:00.000Z",
    open: 234.51,
    high: 234.48,
    low: 233.98,
    close: 234.48,
    volume: 4045,
    turnover: 947305350
  },
  {
    timestamp: "2015-09-27T02:00:00.000Z",
    open: 234.48,
    high: 234.76,
    low: 234.45,
    close: 234.76,
    volume: 5000,
    turnover: 1172870000
  },
  {
    timestamp: "2015-09-27T03:00:00.000Z",
    open: 234.76,
    high: 234.76,
    low: 234.76,
    close: 234.76,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-27T04:00:00.000Z",
    open: 234.76,
    high: 234.5,
    low: 234.18,
    close: 234.5,
    volume: 200,
    turnover: 46868000
  },
  {
    timestamp: "2015-09-27T05:00:00.000Z",
    open: 234.5,
    high: 234.5,
    low: 234.5,
    close: 234.5,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-27T06:00:00.000Z",
    open: 234.5,
    high: 234.5,
    low: 234.5,
    close: 234.5,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-27T07:00:00.000Z",
    open: 234.5,
    high: 234.44,
    low: 234,
    close: 234.44,
    volume: 1025,
    turnover: 240276600
  },
  {
    timestamp: "2015-09-27T08:00:00.000Z",
    open: 234.44,
    high: 234.44,
    low: 234.44,
    close: 234.44,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-27T09:00:00.000Z",
    open: 234.44,
    high: 234.31,
    low: 233.9,
    close: 233.98,
    volume: 9063,
    turnover: 2120557910
  },
  {
    timestamp: "2015-09-27T10:00:00.000Z",
    open: 233.98,
    high: 233.98,
    low: 233.98,
    close: 233.98,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-27T11:00:00.000Z",
    open: 233.98,
    high: 234.2,
    low: 233.68,
    close: 233.8,
    volume: 9000,
    turnover: 2104995000
  },
  {
    timestamp: "2015-09-27T12:00:00.000Z",
    open: 233.8,
    high: 234.13,
    low: 233.12,
    close: 233.49,
    volume: 56040,
    turnover: 13087698500
  },
  {
    timestamp: "2015-09-27T13:00:00.000Z",
    open: 233.49,
    high: 233.49,
    low: 233.21,
    close: 233.21,
    volume: 6131,
    turnover: 1430744600
  },
  {
    timestamp: "2015-09-27T14:00:00.000Z",
    open: 233.21,
    high: 233.37,
    low: 233.03,
    close: 233.28,
    volume: 12999,
    turnover: 3031016550
  },
  {
    timestamp: "2015-09-27T15:00:00.000Z",
    open: 233.28,
    high: 233.28,
    low: 233.28,
    close: 233.28,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-27T16:00:00.000Z",
    open: 233.28,
    high: 233.23,
    low: 233.23,
    close: 233.23,
    volume: 900,
    turnover: 209907000
  },
  {
    timestamp: "2015-09-27T17:00:00.000Z",
    open: 233.23,
    high: 233.68,
    low: 233.32,
    close: 233.32,
    volume: 8000,
    turnover: 1867520000
  },
  {
    timestamp: "2015-09-27T18:00:00.000Z",
    open: 233.32,
    high: 233.13,
    low: 233.13,
    close: 233.13,
    volume: 2000,
    turnover: 466260000
  },
  {
    timestamp: "2015-09-27T19:00:00.000Z",
    open: 233.13,
    high: 233.23,
    low: 233.23,
    close: 233.23,
    volume: 1,
    turnover: 233230
  },
  {
    timestamp: "2015-09-27T20:00:00.000Z",
    open: 233.23,
    high: 233.48,
    low: 233.37,
    close: 233.48,
    volume: 6150,
    turnover: 1435680980
  },
  {
    timestamp: "2015-09-27T21:00:00.000Z",
    open: 233.48,
    high: 233.47,
    low: 233.44,
    close: 233.44,
    volume: 35,
    turnover: 8170700
  },
  {
    timestamp: "2015-09-27T22:00:00.000Z",
    open: 233.44,
    high: 233.51,
    low: 233.1,
    close: 233.41,
    volume: 2979,
    turnover: 694706090
  },
  {
    timestamp: "2015-09-27T23:00:00.000Z",
    open: 233.41,
    high: 233.65,
    low: 233.35,
    close: 233.35,
    volume: 1849,
    turnover: 431863990
  },
  {
    timestamp: "2015-09-28T00:00:00.000Z",
    open: 233.35,
    high: 233.35,
    low: 233.29,
    close: 233.29,
    volume: 1500,
    turnover: 349965000
  },
  {
    timestamp: "2015-09-28T01:00:00.000Z",
    open: 233.29,
    high: 233.04,
    low: 232.9,
    close: 233.04,
    volume: 2950,
    turnover: 687210000
  },
  {
    timestamp: "2015-09-28T02:00:00.000Z",
    open: 233.04,
    high: 233.93,
    low: 233.39,
    close: 233.58,
    volume: 3072,
    turnover: 717900880
  },
  {
    timestamp: "2015-09-28T03:00:00.000Z",
    open: 233.58,
    high: 234.58,
    low: 233.6,
    close: 234.58,
    volume: 13170,
    turnover: 3080006600
  },
  {
    timestamp: "2015-09-28T04:00:00.000Z",
    open: 234.58,
    high: 238.23,
    low: 234.45,
    close: 237.47,
    volume: 57314,
    turnover: 13532590620
  },
  {
    timestamp: "2015-09-28T05:00:00.000Z",
    open: 237.47,
    high: 237.82,
    low: 237.34,
    close: 237.51,
    volume: 6478,
    turnover: 1539245010
  },
  {
    timestamp: "2015-09-28T06:00:00.000Z",
    open: 237.51,
    high: 237.8,
    low: 237.37,
    close: 237.8,
    volume: 8920,
    turnover: 2119060400
  },
  {
    timestamp: "2015-09-28T07:00:00.000Z",
    open: 237.8,
    high: 237.24,
    low: 237.03,
    close: 237.24,
    volume: 40,
    turnover: 9485400
  },
  {
    timestamp: "2015-09-28T08:00:00.000Z",
    open: 237.24,
    high: 238.7,
    low: 237.3,
    close: 238.7,
    volume: 23333,
    turnover: 5560657320
  },
  {
    timestamp: "2015-09-28T09:00:00.000Z",
    open: 238.7,
    high: 240.49,
    low: 238.2,
    close: 238.4,
    volume: 39390,
    turnover: 9412507990
  },
  {
    timestamp: "2015-09-28T10:00:00.000Z",
    open: 238.4,
    high: 238.6,
    low: 238.31,
    close: 238.6,
    volume: 16924,
    turnover: 4034242200
  },
  {
    timestamp: "2015-09-28T11:00:00.000Z",
    open: 238.6,
    high: 239.16,
    low: 238.57,
    close: 239.16,
    volume: 7521,
    turnover: 1798539080
  },
  {
    timestamp: "2015-09-28T13:00:00.000Z",
    open: 239.16,
    high: 238,
    low: 238,
    close: 238,
    volume: 500,
    turnover: 119000000
  },
  {
    timestamp: "2015-09-28T14:00:00.000Z",
    open: 238,
    high: 238.19,
    low: 237.87,
    close: 238.19,
    volume: 15650,
    turnover: 3724476990
  },
  {
    timestamp: "2015-09-28T15:00:00.000Z",
    open: 238.19,
    high: 239.7,
    low: 238.15,
    close: 239.15,
    volume: 25249,
    turnover: 6032412250
  },
  {
    timestamp: "2015-09-28T16:00:00.000Z",
    open: 239.15,
    high: 239.31,
    low: 238.8,
    close: 239.31,
    volume: 1523,
    turnover: 364294620
  },
  {
    timestamp: "2015-09-28T17:00:00.000Z",
    open: 239.31,
    high: 239,
    low: 239,
    close: 239,
    volume: 2189,
    turnover: 523171000
  },
  {
    timestamp: "2015-09-28T18:00:00.000Z",
    open: 239,
    high: 238.87,
    low: 238.61,
    close: 238.61,
    volume: 4160,
    turnover: 993148800
  },
  {
    timestamp: "2015-09-28T19:00:00.000Z",
    open: 238.61,
    high: 239,
    low: 239,
    close: 239,
    volume: 7,
    turnover: 1673000
  },
  {
    timestamp: "2015-09-28T20:00:00.000Z",
    open: 239,
    high: 238.98,
    low: 238.38,
    close: 238.38,
    volume: 11,
    turnover: 2622780
  },
  {
    timestamp: "2015-09-28T21:00:00.000Z",
    open: 238.38,
    high: 238.38,
    low: 238.38,
    close: 238.38,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-28T22:00:00.000Z",
    open: 238.38,
    high: 243.19,
    low: 238.4,
    close: 240.26,
    volume: 2171,
    turnover: 518082650
  },
  {
    timestamp: "2015-09-28T23:00:00.000Z",
    open: 240.26,
    high: 240.26,
    low: 238.25,
    close: 238.25,
    volume: 3239,
    turnover: 774464450
  },
  {
    timestamp: "2015-09-29T00:00:00.000Z",
    open: 238.25,
    high: 240.63,
    low: 240.07,
    close: 240.07,
    volume: 34684,
    turnover: 8338266360
  },
  {
    timestamp: "2015-09-29T01:00:00.000Z",
    open: 240.07,
    high: 241.5,
    low: 238.34,
    close: 238.34,
    volume: 34255,
    turnover: 8234296900
  },
  {
    timestamp: "2015-09-29T02:00:00.000Z",
    open: 238.34,
    high: 239.7,
    low: 238.64,
    close: 239,
    volume: 20526,
    turnover: 4909021330
  },
  {
    timestamp: "2015-09-29T03:00:00.000Z",
    open: 239,
    high: 239.7,
    low: 239.7,
    close: 239.7,
    volume: 1000,
    turnover: 239700000
  },
  {
    timestamp: "2015-09-29T04:00:00.000Z",
    open: 239.7,
    high: 239.57,
    low: 239.09,
    close: 239.14,
    volume: 5401,
    turnover: 1292041140
  },
  {
    timestamp: "2015-09-29T05:00:00.000Z",
    open: 239.14,
    high: 240.52,
    low: 239.6,
    close: 240.52,
    volume: 6544,
    turnover: 1570432740
  },
  {
    timestamp: "2015-09-29T06:00:00.000Z",
    open: 240.52,
    high: 240.76,
    low: 240.06,
    close: 240.7,
    volume: 6937,
    turnover: 1667398220
  },
  {
    timestamp: "2015-09-29T07:00:00.000Z",
    open: 240.7,
    high: 240.7,
    low: 240.7,
    close: 240.7,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-29T08:00:00.000Z",
    open: 240.7,
    high: 240.87,
    low: 239.96,
    close: 240.13,
    volume: 36000,
    turnover: 8660827000
  },
  {
    timestamp: "2015-09-29T09:00:00.000Z",
    open: 240.13,
    high: 239.96,
    low: 238.88,
    close: 238.96,
    volume: 14770,
    turnover: 3532539880
  },
  {
    timestamp: "2015-09-29T10:00:00.000Z",
    open: 238.96,
    high: 238.99,
    low: 237.59,
    close: 237.77,
    volume: 15574,
    turnover: 3708101650
  },
  {
    timestamp: "2015-09-29T11:00:00.000Z",
    open: 237.77,
    high: 237.51,
    low: 237.51,
    close: 237.51,
    volume: 17,
    turnover: 4037670
  },
  {
    timestamp: "2015-09-29T12:00:00.000Z",
    open: 237.51,
    high: 237.88,
    low: 236.5,
    close: 236.5,
    volume: 31786,
    turnover: 7531695570
  },
  {
    timestamp: "2015-09-29T13:00:00.000Z",
    open: 236.5,
    high: 236.58,
    low: 235.81,
    close: 236.58,
    volume: 9381,
    turnover: 2215412560
  },
  {
    timestamp: "2015-09-29T14:00:00.000Z",
    open: 236.58,
    high: 237.34,
    low: 236.64,
    close: 237.34,
    volume: 9010,
    turnover: 2135821400
  },
  {
    timestamp: "2015-09-29T15:00:00.000Z",
    open: 237.34,
    high: 237.87,
    low: 237.17,
    close: 237.83,
    volume: 11003,
    turnover: 2611711770
  },
  {
    timestamp: "2015-09-29T16:00:00.000Z",
    open: 237.83,
    high: 238.18,
    low: 237.68,
    close: 238.18,
    volume: 12900,
    turnover: 3068992000
  },
  {
    timestamp: "2015-09-29T17:00:00.000Z",
    open: 238.18,
    high: 237.97,
    low: 237.97,
    close: 237.97,
    volume: 2500,
    turnover: 594925000
  },
  {
    timestamp: "2015-09-29T18:00:00.000Z",
    open: 237.97,
    high: 238.41,
    low: 238.15,
    close: 238.41,
    volume: 4300,
    turnover: 1025065000
  },
  {
    timestamp: "2015-09-29T19:00:00.000Z",
    open: 238.41,
    high: 238.17,
    low: 236.97,
    close: 237,
    volume: 30882,
    turnover: 7329915900
  },
  {
    timestamp: "2015-09-29T20:00:00.000Z",
    open: 237,
    high: 237.39,
    low: 237,
    close: 237.39,
    volume: 9740,
    turnover: 2310988600
  },
  {
    timestamp: "2015-09-29T21:00:00.000Z",
    open: 237.39,
    high: 237.39,
    low: 237.39,
    close: 237.39,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-29T22:00:00.000Z",
    open: 237.39,
    high: 237.39,
    low: 237.39,
    close: 237.39,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-29T23:00:00.000Z",
    open: 237.39,
    high: 237.35,
    low: 237.2,
    close: 237.2,
    volume: 10000,
    turnover: 2372775000
  },
  {
    timestamp: "2015-09-30T00:00:00.000Z",
    open: 237.2,
    high: 237.5,
    low: 237,
    close: 237,
    volume: 26500,
    turnover: 6285375000
  },
  {
    timestamp: "2015-09-30T01:00:00.000Z",
    open: 237,
    high: 237.15,
    low: 236.88,
    close: 236.88,
    volume: 3760,
    turnover: 891172480
  },
  {
    timestamp: "2015-09-30T02:00:00.000Z",
    open: 236.88,
    high: 237.91,
    low: 237.77,
    close: 237.91,
    volume: 5413,
    turnover: 1287380920
  },
  {
    timestamp: "2015-09-30T03:00:00.000Z",
    open: 237.91,
    high: 238.2,
    low: 237.56,
    close: 237.56,
    volume: 4100,
    turnover: 975260400
  },
  {
    timestamp: "2015-09-30T04:00:00.000Z",
    open: 237.56,
    high: 237.56,
    low: 237.56,
    close: 237.56,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-09-30T05:00:00.000Z",
    open: 237.56,
    high: 238.11,
    low: 238.06,
    close: 238.09,
    volume: 6033,
    turnover: 1436315970
  },
  {
    timestamp: "2015-09-30T06:00:00.000Z",
    open: 238.09,
    high: 237.89,
    low: 237.59,
    close: 237.59,
    volume: 1870,
    turnover: 444413300
  },
  {
    timestamp: "2015-09-30T07:00:00.000Z",
    open: 237.59,
    high: 238.12,
    low: 237.71,
    close: 237.95,
    volume: 15276,
    turnover: 3636126950
  },
  {
    timestamp: "2015-09-30T08:00:00.000Z",
    open: 237.95,
    high: 236.7,
    low: 236.57,
    close: 236.66,
    volume: 9600,
    turnover: 2271978610
  },
  {
    timestamp: "2015-09-30T09:00:00.000Z",
    open: 236.66,
    high: 236.26,
    low: 235.56,
    close: 236,
    volume: 24185,
    turnover: 5703666600
  },
  {
    timestamp: "2015-09-30T10:00:00.000Z",
    open: 236,
    high: 236.16,
    low: 236.14,
    close: 236.14,
    volume: 608,
    turnover: 143583240
  },
  {
    timestamp: "2015-09-30T11:00:00.000Z",
    open: 236.14,
    high: 236.25,
    low: 236.18,
    close: 236.18,
    volume: 9555,
    turnover: 2257049900
  },
  {
    timestamp: "2015-09-30T12:00:00.000Z",
    open: 236.18,
    high: 236.27,
    low: 236.27,
    close: 236.27,
    volume: 10,
    turnover: 2362700
  },
  {
    timestamp: "2015-09-30T13:00:00.000Z",
    open: 236.27,
    high: 236.81,
    low: 236.3,
    close: 236.81,
    volume: 39484,
    turnover: 9340792370
  },
  {
    timestamp: "2015-09-30T14:00:00.000Z",
    open: 236.81,
    high: 237.05,
    low: 236.61,
    close: 236.96,
    volume: 31180,
    turnover: 7384496200
  },
  {
    timestamp: "2015-09-30T15:00:00.000Z",
    open: 236.96,
    high: 237.16,
    low: 236.86,
    close: 237.16,
    volume: 11520,
    turnover: 2729880800
  },
  {
    timestamp: "2015-09-30T16:00:00.000Z",
    open: 237.16,
    high: 237.15,
    low: 237.07,
    close: 237.15,
    volume: 6880,
    turnover: 1631328000
  },
  {
    timestamp: "2015-09-30T17:00:00.000Z",
    open: 237.15,
    high: 237.65,
    low: 237.32,
    close: 237.48,
    volume: 23460,
    turnover: 5573438290
  },
  {
    timestamp: "2015-09-30T18:00:00.000Z",
    open: 237.48,
    high: 237.91,
    low: 237.65,
    close: 237.76,
    volume: 13565,
    turnover: 3225125280
  },
  {
    timestamp: "2015-09-30T19:00:00.000Z",
    open: 237.76,
    high: 238.07,
    low: 237.29,
    close: 237.29,
    volume: 20900,
    turnover: 4967416000
  },
  {
    timestamp: "2015-09-30T20:00:00.000Z",
    open: 237.29,
    high: 237.23,
    low: 237,
    close: 237.23,
    volume: 420,
    turnover: 99544600
  },
  {
    timestamp: "2015-09-30T21:00:00.000Z",
    open: 237.23,
    high: 237.2,
    low: 237.01,
    close: 237.01,
    volume: 5400,
    turnover: 1280804000
  },
  {
    timestamp: "2015-09-30T22:00:00.000Z",
    open: 237.01,
    high: 236.87,
    low: 236.87,
    close: 236.87,
    volume: 4,
    turnover: 947480
  },
  {
    timestamp: "2015-09-30T23:00:00.000Z",
    open: 236.87,
    high: 236.87,
    low: 236.87,
    close: 236.87,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-10-01T00:00:00.000Z",
    open: 236.87,
    high: 237.03,
    low: 236.55,
    close: 236.55,
    volume: 6740,
    turnover: 1595057400
  },
  {
    timestamp: "2015-10-01T01:00:00.000Z",
    open: 236.55,
    high: 236.55,
    low: 236.55,
    close: 236.55,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-10-01T02:00:00.000Z",
    open: 236.55,
    high: 236.98,
    low: 236.57,
    close: 236.98,
    volume: 8000,
    turnover: 1894200000
  },
  {
    timestamp: "2015-10-01T03:00:00.000Z",
    open: 236.98,
    high: 237.69,
    low: 237.51,
    close: 237.69,
    volume: 1100,
    turnover: 261279000
  },
  {
    timestamp: "2015-10-01T04:00:00.000Z",
    open: 237.69,
    high: 237.69,
    low: 237.69,
    close: 237.69,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-10-01T05:00:00.000Z",
    open: 237.69,
    high: 237.47,
    low: 237.47,
    close: 237.47,
    volume: 4000,
    turnover: 949880000
  },
  {
    timestamp: "2015-10-01T06:00:00.000Z",
    open: 237.47,
    high: 237.5,
    low: 237.5,
    close: 237.5,
    volume: 1,
    turnover: 237500
  },
  {
    timestamp: "2015-10-01T07:00:00.000Z",
    open: 237.5,
    high: 237.5,
    low: 237.2,
    close: 237.2,
    volume: 1101,
    turnover: 261337500
  },
  {
    timestamp: "2015-10-01T08:00:00.000Z",
    open: 237.2,
    high: 236.87,
    low: 236.87,
    close: 236.87,
    volume: 915,
    turnover: 216736050
  },
  {
    timestamp: "2015-10-01T09:00:00.000Z",
    open: 236.87,
    high: 236.85,
    low: 236.85,
    close: 236.85,
    volume: 1000,
    turnover: 236850000
  },
  {
    timestamp: "2015-10-01T10:00:00.000Z",
    open: 236.85,
    high: 237.03,
    low: 236.91,
    close: 236.91,
    volume: 5915,
    turnover: 1401922650
  },
  {
    timestamp: "2015-10-01T11:00:00.000Z",
    open: 236.91,
    high: 236.91,
    low: 236.91,
    close: 236.91,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-10-01T12:00:00.000Z",
    open: 236.91,
    high: 237.08,
    low: 237.08,
    close: 237.08,
    volume: 4000,
    turnover: 948320000
  },
  {
    timestamp: "2015-10-01T13:00:00.000Z",
    open: 237.08,
    high: 237.3,
    low: 234.63,
    close: 236.88,
    volume: 11020,
    turnover: 2607593200
  },
  {
    timestamp: "2015-10-01T14:00:00.000Z",
    open: 236.88,
    high: 237,
    low: 236.69,
    close: 236.69,
    volume: 8015,
    turnover: 1898270350
  },
  {
    timestamp: "2015-10-01T15:00:00.000Z",
    open: 236.69,
    high: 237.33,
    low: 237,
    close: 237.33,
    volume: 6222,
    turnover: 1476264000
  },
  {
    timestamp: "2015-10-01T16:00:00.000Z",
    open: 237.33,
    high: 238.94,
    low: 237.81,
    close: 238.94,
    volume: 15684,
    turnover: 3739031750
  },
  {
    timestamp: "2015-10-01T17:00:00.000Z",
    open: 238.94,
    high: 239.53,
    low: 238.65,
    close: 238.65,
    volume: 33378,
    turnover: 7980557820
  },
  {
    timestamp: "2015-10-01T18:00:00.000Z",
    open: 238.65,
    high: 238.37,
    low: 238.37,
    close: 238.37,
    volume: 1000,
    turnover: 238370000
  },
  {
    timestamp: "2015-10-01T19:00:00.000Z",
    open: 238.37,
    high: 238.37,
    low: 238.37,
    close: 238.37,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-10-01T20:00:00.000Z",
    open: 238.37,
    high: 238.69,
    low: 238.69,
    close: 238.69,
    volume: 2000,
    turnover: 477380000
  },
  {
    timestamp: "2015-10-01T21:00:00.000Z",
    open: 238.69,
    high: 238.59,
    low: 238.59,
    close: 238.59,
    volume: 10000,
    turnover: 2385900000
  },
  {
    timestamp: "2015-10-01T22:00:00.000Z",
    open: 238.59,
    high: 238.57,
    low: 238.57,
    close: 238.57,
    volume: 5000,
    turnover: 1192850000
  },
  {
    timestamp: "2015-10-01T23:00:00.000Z",
    open: 238.57,
    high: 238.57,
    low: 238.57,
    close: 238.57,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-10-02T00:00:00.000Z",
    open: 238.57,
    high: 238.3,
    low: 238.1,
    close: 238.1,
    volume: 10000,
    turnover: 2381640000
  },
  {
    timestamp: "2015-10-02T01:00:00.000Z",
    open: 238.1,
    high: 238.15,
    low: 237.86,
    close: 237.86,
    volume: 7000,
    turnover: 1665550000
  },
  {
    timestamp: "2015-10-02T02:00:00.000Z",
    open: 237.86,
    high: 238.99,
    low: 238,
    close: 238.7,
    volume: 5976,
    turnover: 1425561340
  },
  {
    timestamp: "2015-10-02T03:00:00.000Z",
    open: 238.7,
    high: 238.7,
    low: 238.42,
    close: 238.42,
    volume: 7000,
    turnover: 1669500800
  },
  {
    timestamp: "2015-10-02T04:00:00.000Z",
    open: 238.42,
    high: 238.36,
    low: 238.16,
    close: 238.36,
    volume: 3004,
    turnover: 715503440
  },
  {
    timestamp: "2015-10-02T05:00:00.000Z",
    open: 238.36,
    high: 238.24,
    low: 238.2,
    close: 238.2,
    volume: 3,
    turnover: 714680
  },
  {
    timestamp: "2015-10-02T06:00:00.000Z",
    open: 238.2,
    high: 238.2,
    low: 238.2,
    close: 238.2,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-10-02T07:00:00.000Z",
    open: 238.2,
    high: 238.2,
    low: 238.2,
    close: 238.2,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-10-02T08:00:00.000Z",
    open: 238.2,
    high: 238.65,
    low: 238.63,
    close: 238.63,
    volume: 5000,
    turnover: 1193210000
  },
  {
    timestamp: "2015-10-02T09:00:00.000Z",
    open: 238.63,
    high: 238.8,
    low: 238.8,
    close: 238.8,
    volume: 7500,
    turnover: 1791000000
  },
  {
    timestamp: "2015-10-02T10:00:00.000Z",
    open: 238.8,
    high: 238.8,
    low: 238.8,
    close: 238.8,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-10-02T11:00:00.000Z",
    open: 238.8,
    high: 238.74,
    low: 238.55,
    close: 238.55,
    volume: 8800,
    turnover: 2100300000
  },
  {
    timestamp: "2015-10-02T12:00:00.000Z",
    open: 238.55,
    high: 237.69,
    low: 237.69,
    close: 237.69,
    volume: 5000,
    turnover: 1188450000
  },
  {
    timestamp: "2015-10-02T13:00:00.000Z",
    open: 237.69,
    high: 238,
    low: 237.57,
    close: 238,
    volume: 17180,
    turnover: 4084484380
  },
  {
    timestamp: "2015-10-02T14:00:00.000Z",
    open: 238,
    high: 238.22,
    low: 238,
    close: 238.22,
    volume: 2087,
    turnover: 496737020
  },
  {
    timestamp: "2015-10-02T15:00:00.000Z",
    open: 238.22,
    high: 238.22,
    low: 238.22,
    close: 238.22,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-10-02T16:00:00.000Z",
    open: 238.22,
    high: 237.8,
    low: 237.09,
    close: 237.09,
    volume: 15500,
    turnover: 3685045000
  },
  {
    timestamp: "2015-10-02T17:00:00.000Z",
    open: 237.09,
    high: 237.5,
    low: 237.5,
    close: 237.5,
    volume: 500,
    turnover: 118750000
  },
  {
    timestamp: "2015-10-02T18:00:00.000Z",
    open: 237.5,
    high: 237.65,
    low: 237.53,
    close: 237.54,
    volume: 7000,
    turnover: 1663329800
  },
  {
    timestamp: "2015-10-02T19:00:00.000Z",
    open: 237.54,
    high: 237.54,
    low: 237.54,
    close: 237.54,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-10-02T20:00:00.000Z",
    open: 237.54,
    high: 237.54,
    low: 237.54,
    close: 237.54,
    volume: 0,
    turnover: 0
  },
  {
    timestamp: "2015-10-02T21:00:00.000Z",
    open: 237.54,
    high: 237.63,
    low: 237.53,
    close: 237.63,
    volume: 4000,
    turnover: 950320000
  },
  {
    timestamp: "2015-10-02T22:00:00.000Z",
    open: 237.63,
    high: 237.68,
    low: 237.67,
    close: 237.68,
    volume: 4000,
    turnover: 950690000
  },
  {
    timestamp: "2015-10-02T23:00:00.000Z",
    open: 237.68,
    high: 237.67,
    low: 237.67,
    close: 237.67,
    volume: 3000,
    turnover: 713010000
  }
];

temp
  .map((x, i) => (i > 12 ? temp.slice(0, i).slice(-13) : x))
  .slice(13)
  .map(test => {
    const t = test.slice();
    t[12]["MaxIn13"] = test.sort((y, x) => x["high"] - y.high)[0].high;
    t[12]["MinIn13"] = test.sort((x, y) => x["low"] - y.low)[0].low;
    return t[12];
  })
  .map(
    x =>
      (x["rsv"] =
        (-(x["MaxIn13"] - x["close"]) / (x["MaxIn13"] - x["MinIn13"])) * 100)
  );
temp1 = temp;
temp1.map(
  (x, i, array) =>
    (x["lwr1"] =
      i >= 12
        ? i === 12
          ? x["rsv"]
          : (x["rsv"] + 2 * array[i - 1]["lwr1"]) / 3
        : null)
);
temp2 = temp1;
temp3 = temp2.map(
  (x, i, array) =>
    (x["lwr2"] =
      i >= 12
        ? i === 12
          ? x["lwr1"]
          : (x["lwr1"] + 2 * array[i - 1]["lwr2"]) / 3
        : null)
);
temp3 = temp2;
temp3.map(x => (x["action1-1"] = x["lwr1"] >= x["lwr2"] ? "buy" : "sell"));
temp3
  .map((x, i) => (i > 7 ? temp.slice(0, i).slice(-8) : x))
  .slice(8)
  .map(test => {
    const t = test.slice();
    t[7]["MaxIn8"] = test.sort((y, x) => x["high"] - y.high)[0].high;
    t[7]["MinIn8"] = test.sort((x, y) => x["low"] - y.low)[0].low;
    return t[7];
  })
  .map(
    x =>
      (x["rsv1"] =
        ((x["close"] - x["MinIn8"]) / (x["MaxIn8"] - x["MinIn8"])) * 100)
  );
temp4 = temp3;
temp4.map(
  (x, i, array) =>
    (x["k"] =
      i >= 7
        ? i === 7
          ? x["rsv1"]
          : (x["rsv1"] + 2 * array[i - 1]["k"]) / 3
        : null)
);
temp5 = temp4;
temp5.map(
  (x, i, array) =>
    (x["d"] =
      i >= 7 ? (i === 7 ? x["k"] : (x["k"] + 2 * array[i - 1]["d"]) / 3) : null)
);
temp6 = temp5;
temp6.map(x => (x["action1-2"] = x["k"] >= x["d"] ? "buy" : "sell"));
temp6 = temp;
temp6.map((x, i, array) => (x["lc"] = i === 0 ? null : array[i - 1]["close"]));
temp6.map(
  (x, i, array) =>
    (x["rsi1_1"] =
      i > 0
        ? i === 1
          ? Math.max(x["close"] - x["lc"], 0)
          : (Math.max(x["close"] - x["lc"], 0) + 4 * array[i - 1]["rsi1_1"]) / 5
        : null)
);
temp6.map(
  (x, i, array) =>
    (x["rsi1_2"] =
      i > 0
        ? i === 1
          ? Math.abs(x["close"] - x["lc"])
          : (Math.abs(x["close"] - x["lc"]) + 4 * array[i - 1]["rsi1_2"]) / 5
        : null)
);
temp6.map(x => (x["rsi1"] = (x["rsi1_1"] / x["rsi1_2"]) * 100));
temp6.map(
  (x, i, array) =>
    (x["rsi2_1"] =
      i > 0
        ? i === 1
          ? Math.max(x["close"] - x["lc"], 0)
          : (Math.max(x["close"] - x["lc"], 0) + 12 * array[i - 1]["rsi2_1"]) /
            13
        : null)
);
temp6.map(
  (x, i, array) =>
    (x["rsi2_2"] =
      i > 0
        ? i === 1
          ? Math.abs(x["close"] - x["lc"])
          : (Math.abs(x["close"] - x["lc"]) + 12 * array[i - 1]["rsi2_2"]) / 13
        : null)
);
temp6.map(x => (x["rsi2"] = (x["rsi2_1"] / x["rsi2_2"]) * 100));

temp6.map(x => (x["action1-3"] = x["rsi1"] >= x["rsi2"] ? "buy" : "sell"));
temp6.map(
  (x, i, array) =>
    (x["diff_1"] =
      i > 0
        ? i === 1
          ? x["close"]
          : (2 * x["close"] + 7 * array[i - 1]["diff_1"]) / 9
        : null)
);
temp6.map(
  (x, i, array) =>
    (x["diff_2"] =
      i > 0
        ? i === 1
          ? x["close"]
          : (2 * x["close"] + 12 * array[i - 1]["diff_2"]) / 14
        : null)
);
temp6.map(x => (x["diff"] = x["diff_1"] - x["diff_2"]));
temp6.map(
  (x, i, array) =>
    (x["dea"] =
      i > 0
        ? i === 1
          ? x["diff"]
          : (2 * x["diff"] + 4 * array[i - 1]["dea"]) / 6
        : null)
);
temp6.map(x => (x["action1-4"] = x["diff"] >= x["rsa"] ? "buy" : "sell"));
temp6.map(function(x, i, array) {
  if (i > 1) {
    let sum = 0;
    for (let n = i; n > i - 3; n--) {
      sum += array[n]["close"];
    }
    x["bbi_1"] = sum / 3;
  } else {
    return null;
  }
});
temp6.map(function(x, i, array) {
  if (i > 3) {
    let sum = 0;
    for (let n = i; n > i - 5; n--) {
      sum += array[n]["close"];
    }
    x["bbi_2"] = sum / 5;
  } else {
    return null;
  }
});
temp6.map(function(x, i, array) {
  if (i > 6) {
    let sum = 0;
    for (let n = i; n > i - 8; n--) {
      sum += array[n]["close"];
    }
    x["bbi_3"] = sum / 8;
  } else {
    return null;
  }
});
temp6.map(function(x, i, array) {
  if (i > 11) {
    let sum = 0;
    for (let n = i; n > i - 13; n--) {
      sum += array[n]["close"];
    }
    x["bbi_4"] = sum / 13;
  } else {
    return null;
  }
});
temp6.map(
  x => (x["bbi"] = (x["bbi_1"] + x["bbi_2"] + x["bbi_3"] + x["bbi_4"]) / 4)
);
temp6.map(x => (x["action1-5"] = x["close"] >= x["bbi"] ? "buy" : "sell"));
temp6.map((x, i) => (x["mtm"] = i === 0 ? null : x["close"] - x["lc"]));
temp6.map(
  (x, i, array) =>
    (x["mms_1_1"] =
      i > 0
        ? i === 1
          ? x["mtm"]
          : (2 * x["mtm"] + 4 * array[i - 1]["mms_1_1"]) / 6
        : null)
);
temp6.map(
  (x, i, array) =>
    (x["mms_1"] =
      i > 0
        ? i === 1
          ? x["mms_1_1"]
          : (2 * x["mms_1_1"] + 2 * array[i - 1]["mms_1"]) / 4
        : null)
);
temp6.map(
  (x, i, array) =>
    (x["mms_2_1"] =
      i > 0
        ? i === 1
          ? Math.abs(x["mtm"])
          : (2 * Math.abs(x["mtm"]) + 4 * array[i - 1]["mms_2_1"]) / 6
        : null)
);
temp6.map(
  (x, i, array) =>
    (x["mms_2"] =
      i > 0
        ? i === 1
          ? x["mms_2_1"]
          : (2 * x["mms_2_1"] + 2 * array[i - 1]["mms_2"]) / 4
        : null)
);
temp6.map(x => (x["mms"] = 100 * (x["mms_1"] / x["mms_2"])));
temp6.map(
  (x, i, array) =>
    (x["mmm_1_1"] =
      i > 0
        ? i === 1
          ? x["mtm"]
          : (2 * x["mtm"] + 12 * array[i - 1]["mmm_1_1"]) / 14
        : null)
);
temp6.map(
  (x, i, array) =>
    (x["mmm_1"] =
      i > 0
        ? i === 1
          ? x["mmm_1_1"]
          : (2 * x["mmm_1_1"] + 7 * array[i - 1]["mmm_1"]) / 9
        : null)
);
temp6.map(
  (x, i, array) =>
    (x["mmm_2_1"] =
      i > 0
        ? i === 1
          ? Math.abs(x["mtm"])
          : (2 * Math.abs(x["mtm"]) + 12 * array[i - 1]["mmm_2_1"]) / 14
        : null)
);
temp6.map(
  (x, i, array) =>
    (x["mmm_2"] =
      i > 0
        ? i === 1
          ? x["mmm_2_1"]
          : (2 * x["mmm_2_1"] + 7 * array[i - 1]["mmm_2"]) / 9
        : null)
);
temp6.map(x => (x["mmm"] = 100 * (x["mmm_1"] / x["mmm_2"])));
temp6.map(x => (x["action1-6"] = x["mms"] >= x["mmm"] ? "buy" : "sell"));
temp6.map(x => (x["wj"] = (x["high"] + x["low"] + x["close"]) / 3));
temp6.map(
  x =>
    (x["v1"] =
      x["high"] === x["low"] ? 1 : x["high"] - Math.max(x["open"], x["close"]))
);
temp6.map(
  x =>
    (x["v2"] =
      x["high"] === x["low"] ? 1 : Math.max(x["open"], x["close"]) - x["wj"])
);
temp6.map(
  x =>
    (x["v3"] =
      x["high"] === x["low"] ? 1 : Math.min(x["open"], x["close"]) - x["low"])
);
temp6.map(
  x =>
    (x["v4"] =
      x["high"] === x["low"] ? 1 : x["wj"] - Math.min(x["open"], x["close"]))
);
temp6.map(
  x =>
    (x["v5"] =
      x["volume"] / (x["high"] === x["low"] ? 4 : x["high"] - x["low"]))
);
temp6.map(x => (x["v6"] = x["v1"] * x["v5"]));
temp6.map(x => (x["v7"] = x["v2"] * x["v5"]));
temp6.map(x => (x["v8"] = x["v3"] * x["v5"]));
temp6.map(x => (x["v9"] = x["v4"] * x["v5"]));
temp6.map(x => (x["ddx"] = x["v9"] + x["v8"] - (x["v6"] + x["v7"])));
temp6.map(function(x, i, array) {
  if (i >= 4) {
    let sum = 0;
    for (let n = i; n >= i - 4; n--) {
      sum += array[n]["ddx"];
    }
    x["dx_1"] = sum / 5;
  } else {
    return null;
  }
});
temp6.map(x => (x["dx"] = x["dx_1"] * 20));
temp6.map(
  (x, i, array) =>
    (x["ddx1"] =
      i >= 4
        ? i === 4
          ? x["dx"]
          : (x["dx"] + 2 * array[i - 1]["ddx1"]) / 3
        : null)
);
temp6.map(
  (x, i, array) =>
    (x["ddx2"] =
      i >= 4
        ? i === 4
          ? x["ddx1"]
          : (x["ddx1"] + 2 * array[i - 1]["ddx2"]) / 3
        : null)
);
temp6.map(x => (x["action1-7"] = x["ddx1"] >= x["ddx2"] ? "buy" : "sell"));
temp6.map(x => (x["huanshou"] = x["volume"] / (x["close"] * 80000000000)));
temp6.map(
  x =>
    (x["var1"] =
      x["volume"] === 0
        ? 0
        : x["volume"] /
          ((x["high"] - x["low"]) * 2 - Math.abs(x["close"] - x["open"])))
);
temp6.map(
  x =>
    (x["maipanA1_1"] =
      x["close"] > x["open"]
        ? x["var1"] * (x["high"] - x["open"] + (x["close"] - x["low"]))
        : x["volume"] / 2)
);
temp6.map(
  x =>
    (x["maipanA1"] =
      x["close"] > x["open"]
        ? (x["high"] - x["low"]) * x["var1"]
        : x["maipanA1_1"])
);
temp6.map(
  x =>
    (x["maipanB1_1"] =
      x["close"] > x["open"]
        ? 0 - x["var1"] * (x["high"] - x["low"])
        : 0 - x["volume"] / 2)
);
temp6.map(
  x =>
    (x["maipanB1"] =
      x["close"] > x["open"]
        ? 0 - x["var1"] * (x["high"] - x["close"] + (x["open"] - x["low"]))
        : x["maipanB1_1"])
);
temp6.map(
  x =>
    (x["dlx"] =
      x["volume"] === 0
        ? 0
        : ((x["maipanA1"] - -x["maipanB1"]) / x["volume"]) * x["huanshou"])
);
temp6.map(function(x, i, array) {
  if (i > 4) {
    let sum = 0;
    for (let n = i; n > i - 6; n--) {
      sum += array[n]["dlx"];
    }
    x["dlx1"] = sum;
  } else {
    return null;
  }
});
temp6.map(function(x, i, array) {
  if (i > 8) {
    let sum = 0;
    for (let n = i; n > i - 5; n--) {
      sum += array[n]["dlx1"];
    }
    x["dlx2"] = sum / 5;
  } else {
    return null;
  }
});
temp6.map(x => (x["action1-8"] = x["dlx1"] >= x["dlx2"] ? "buy" : "sell"));

temp6.map(
  x =>
    (x["mid"] =
      (3 * x["close"] + 1 * x["low"] + 1 * x["open"] + 1 * x["high"]) / 6)
);

temp6.map(function(x, i, array) {
  if (i > 19) {
    let sum = 0;
    for (let n = i; n > i - 19; n--) {
      sum += (20 - i + n) * array[n]["mid"];
    }
    x["zhuli"] = (1 * sum + 1 * array[i - 20]["mid"]) / 210;
  } else {
    return null;
  }
});

temp6.map(x => (x["action2-1"] = x["zhuli"] >= 1 ? "buy" : "sell"));

temp6.map(function(x) {
  currentAction1 = [
    x["action1-1"],
    x["action1-2"],
    x["action1-3"],
    x["action1-4"],
    x["action1-5"],
    x["action1-6"],
    x["action1-7"],
    x["action1-8"]
  ];
  x["actionMaxA"] =
    currentAction1.filter(y => y === "buy").length >= 7 ? "buy" : "sell";
  x["actionMinA"] =
    currentAction1.filter(y => y === "sell").length === 8 ? "sell" : "buy";
});
